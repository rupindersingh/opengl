//Rupinder :D

#include<iostream>
#include<cmath>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
#include<GL/glut.h>
#include<pthread.h>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define toradian(x) (x*Pi/180)
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}

vector<pair<float,float> > sp_v;
vector<float> sp_vel,laser_angle,sp_col;
vector<pair<float,float> > laser_v,laser_vel;
float box_len = 4.0f;
float tri_x = 0.0f;
float tri_y = 0.0f;
float theta = 0.0f; 
double mouse_x,mouse_y;
float canon_x = 0.0f; 
float canon_y = -box_len/2; 
float canon_rot = 0.0f; 
int score=0;
int minvel=10,elapsed_time=0,dragl=0,dragr=0;
int maxvel=30,pause=0;
float windowWidth;
float windowHeight;
float w,h,lbox_x=-box_len/2,rbox_x=box_len/2,mouse2_x,mouse2_y;
int control=0;
void drawSpider(void);
void drawBox(float);
void drawCanon(void);
void GetOGLPos(float x, float y,GLdouble* resx,GLdouble* resy)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;
	glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
	glGetDoublev( GL_PROJECTION_MATRIX, projection );
	glGetIntegerv( GL_VIEWPORT, viewport );
	winX = (float)x;
	winY = (float)viewport[3] - (float)y;
	glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

	gluUnProject( winX, winY, 0.0f, modelview, projection, viewport, &posX, &posY, &posZ);
	*resx=posX;
	*resy=posY;
	return;
}
void mouse1(int x, int y)
{

	mouse_x=float(x);
	mouse_y=(float)y;
	glPushMatrix();
	GLdouble t_x,t_y;
	GetOGLPos(mouse_x,mouse_y,&t_x,&t_y);
	mouse2_x=5*t_x;
	mouse2_y=5*t_y;
	glTranslatef(canon_x/5,canon_y/5,0.0);
	GetOGLPos(mouse_x,mouse_y,&t_x,&t_y);
	mouse_x=t_x;
	mouse_y=t_y;
	glPopMatrix();
}
void drawCylinder(int numMajor, int numMinor, float height, float radius)
{
	double majorStep = height / numMajor;
	double minorStep = 2.0 * M_PI / numMinor;
	int i, j;

	for (i = 0; i < numMajor; ++i) {
		float z0 = 0.5 * height - i * majorStep;
		float z1 = z0 - majorStep;

		glBegin(GL_TRIANGLE_STRIP);
		for (j = 0; j <= numMinor; ++j) {
			double a = j * minorStep;
			float x = radius * cos(a);
			float y = radius * sin(a);
			glNormal3f(x / radius, y / radius, 0.0);
			glTexCoord2f(j / numMinor, i / numMajor);
			glVertex3f(x, y, z0);

			glTexCoord2f(j / numMinor, (i + 1) / numMajor);
			glVertex3f(x, y, z1);
		}
		glEnd();
	}
}
void drawlaser(int angle)
{
	glBegin(GL_LINES);
	glColor3f((float)(rand()%1000)/1000,(float)(rand()%1000)/1000,(float)(rand()%1000)/1000);
	glColor3f(0,0,0);
	glVertex2f(0.0f,0.0f);
	glVertex2f((box_len/10)*cos(angle*Pi/180),(box_len/10)*sin(angle*Pi/180));
	glEnd();
}
void drawLbox(){
	glPushMatrix();
	glTranslatef(rbox_x,-box_len/2,0);
	glBegin(GL_QUADS);
	glColor3f(0.0,1.0f,0.0f);
	glVertex3f(-0.2,0.2,0);
	glVertex3f(0.2,0.2,0);
	glVertex3f(0.2,-0.2,0);
	glVertex3f(-0.2,-0.2,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,.2,0);
	glColor3f(.5,.5,.5);
	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<360 ; i++) 
		glVertex2f(0.2 * cos(toradian(i)), 0.05 * sin(toradian(i)));
	glEnd();
	glPopMatrix();

	glPopMatrix();
}
void drawRbox(){
	glPushMatrix();
	glTranslatef(lbox_x,-box_len/2,0);
	glColor3f(1.0f,0.0f,0.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.2,.2,0);
	glVertex3f(.2,.2,0);
	glVertex3f(.2,-.2,0);
	glVertex3f(-.2,-.2,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,.2,0);
	glColor3f(.5,.5,.5);
	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<360 ; i++) 
		glVertex2f(0.2 * cos(toradian(i)), 0.05 * sin(toradian(i)));
	glEnd();
	glPopMatrix();
	glPopMatrix();
}
void drawbase(){
	glPushMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glColor3f(34.0/255,139.0/255,34.0/255);
	glTranslatef(0,-box_len/2,0);
	glBegin(GL_QUADS);
	glVertex2f(-box_len/2,0);
	glVertex2f(-box_len/2,.2);
	glVertex2f(box_len/2,.2);
	glVertex2f(box_len/2,0);
	glEnd();
	glPopMatrix();
}
void drawReload(){
	glPushMatrix();
	glColor3f(0,1,0);
	glTranslatef(canon_x,canon_y,0);
	glBegin(GL_LINES);
	glVertex2f(-.5,0);
	glVertex2f(-.5+1*((float)elapsed_time/100),0);
	glEnd();
	glPopMatrix();
}
void drawScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glTranslatef(0,0,-5);
	glColor3f(1.0,1.0,1.0);
	//	glutBitmapCharacter(GLUT_STROKE_ROMAN, 'W');
	glPopMatrix();
	glPushMatrix();
	// Draw Box
	glTranslatef(0.0f, 0.0f, -5.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	drawBox(box_len);
	drawbase();
	drawCanon();
	drawRbox();
	drawLbox();
	drawCylinder();
	drawReload();
	// Draw Spiders
	unsigned int i;
	for(i=0;i<sp_v.size();i++)
	{
		glPushMatrix();
		glTranslatef(sp_v[i].ff, sp_v[i].ss, 0.0f);
		if(sp_col[i]==1)
			glColor3f(0.0f, 1.0f, 0.0f);
		else if(sp_col[i]==0)
			glColor3f(1.0f, 0.0f, 0.0f);
		else if(sp_col[i]==2)
			glColor3f(0.0f, 0.0f, 0.0f);
		drawSpider();
		glPopMatrix();
	}
	//Draw Lasers
	for(i=0;i<laser_v.size();i++)
	{
		glPushMatrix();
		glTranslatef(laser_v[i].ff, laser_v[i].ss, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		drawlaser(laser_angle[i]);
		glPopMatrix();
	}
	glPopMatrix();
	glPushMatrix();
	glMatrixMode( GL_PROJECTION ) ;
	glPushMatrix() ; // save
	glLoadIdentity();// and clear
	glMatrixMode( GL_MODELVIEW ) ;
	glPushMatrix() ;
	glLoadIdentity() ;

	glDisable( GL_DEPTH_TEST ) ; 
	//	glRasterPos3f(1.5,1.5,-0.05);
	glColor3f(0.0f, 0.0f, 0.0f);
	glRasterPos2f(0.1,0.5);
	char buf[300];
	sprintf( buf,"Your Score: %d", score ) ;
	const char * p = buf ;
	do glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18, *p ); while( *(++p) ) ;
	glEnable( GL_DEPTH_TEST ) ;
	glMatrixMode( GL_PROJECTION ) ;
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW ) ;
	glPopMatrix();
	glPopMatrix();
	glutSwapBuffers();
}
void increasedif(int value){
	maxvel+=1;
	minvel+=1;
	glutTimerFunc(5000, increasedif, 0);
}
void update(int value) {
	//cout<<pause<<endl;
	if (pause==1)
	{glutTimerFunc(10, update, 0);
		return;
	}
	if(elapsed_time<=100)
		elapsed_time++;
	unsigned int i;
	//handle drag
	//	cout<<mouse2_x<<endl;
	bool flr=1,fll=1;
	for(unsigned int i=0;i<sp_v.sz;i++)
	{
		if(sp_vel[i]==0&&fabs(sp_v[i].ff-rbox_x)<=.4&&sp_v[i].ff>=rbox_x&&mouse2_x>=rbox_x)
			flr=0;
		if(sp_vel[i]==0&&fabs(sp_v[i].ff-rbox_x)<=.4&&sp_v[i].ff<=rbox_x&&mouse2_x<=rbox_x)
			flr=0;
		if(sp_vel[i]==0&&fabs(sp_v[i].ff-lbox_x)<=.4&&sp_v[i].ff>=lbox_x&&mouse2_x>=lbox_x)
			fll=0;
		if(sp_vel[i]==0&&fabs(sp_v[i].ff-lbox_x)<=.4&&sp_v[i].ff<=lbox_x&&mouse2_x<=lbox_x)
			fll=0;
	}
	if(mouse2_x<-box_len/2||mouse2_x>box_len/2)
	{
		flr=0;
		fll=0;
	}
	if (dragr&&flr)
		rbox_x=mouse2_x;
	if(dragl&&fll)
		lbox_x=mouse2_x;

	//spiders
	for(i=0;i<sp_v.sz;i++)
	{
		sp_v[i].ss-=sp_vel[i];
		if(sp_v[i].ss<-box_len/2){
			if(sp_vel[i])
				score-=5;
			sp_vel[i]=0;
		}
		if(sp_vel[i]!=0&&(pow(rbox_x-sp_v[i].ff,2)+pow(-box_len/2-sp_v[i].ss,2)<=.1))		{

			if(sp_col[i]==1)
				score++;
			else
				score--;
			sp_vel.erase(sp_vel.begin()+i);
			sp_col.erase(sp_col.begin()+i);
			sp_v.erase(sp_v.begin()+i);
		}
		if(sp_vel[i]!=0&&(pow(lbox_x-sp_v[i].ff,2)+pow(-box_len/2-sp_v[i].ss,2)<=.1))
		{
			if(sp_col[i]==0){
				score++;
			}else{
				score--;
			}
			sp_vel.erase(sp_vel.begin()+i);
			sp_col.erase(sp_col.begin()+i);
			sp_v.erase(sp_v.begin()+i);
		}
		if(sp_vel[i]!=0&&(pow(sp_v[i].ss-canon_y,2)+pow(sp_v[i].ff-canon_x,2)<=.1))
		{
			cout<<"Your Score is: "<<score<<endl;
			exit(0);
		}
	}
	for(i=0;i<laser_v.sz;i++)
	{
		laser_v[i].ss+=laser_vel[i].ss;
		laser_v[i].ff+=laser_vel[i].ff;
		if(laser_v[i].ss<-box_len/2||laser_v[i].ss>box_len/2)
		{
			laser_v.erase(laser_v.begin()+i);
			laser_vel.erase(laser_vel.begin()+i);
			laser_angle.erase(laser_angle.begin()+i);
		}
		if(laser_v[i].ff<-box_len/2||laser_v[i].ff>box_len/2)
		{
			laser_angle[i]+=180-2*laser_angle[i];
			laser_vel[i].ff*=-1;
		}
		for(unsigned int j=0;j<sp_v.sz;j++)
		{
			if(pow(laser_v[i].ff-sp_v[j].ff,2)+pow(laser_v[i].ss-sp_v[j].ss,2)<=.1&&sp_vel[j]!=0)
			{
				if(sp_col[j]==2)
					score++;
				else
					score--;
				laser_v.erase(laser_v.begin()+i);
				laser_vel.erase(laser_vel.begin()+i);
				laser_angle.erase(laser_angle.begin()+i);
				sp_v.erase(sp_v.begin()+j);
				sp_vel.erase(sp_vel.begin()+j);
				sp_col.erase(sp_col.begin()+j);
			}
		}
	}
	//cout<<mouse_x<<" "<<mouse_y<<endl;
	glutTimerFunc(10, update, 0);
}
void generate_spider(int scrap)
{
	if (pause==1)
	{
		glutTimerFunc(2000, generate_spider,0);
		return;
	}
	sp_v.pb(mp(-box_len/2+box_len*((float)(rand()%1000)/1000),box_len/2));
	//sp_v.pb(mp(0.0f,0.0f));
	sp_vel.pb((float)(minvel+(float)(rand()%(maxvel-minvel)))/1000);
	sp_col.pb(rand()%3);
	glutTimerFunc(2000, generate_spider,0);
}
void drawBox(float len) {
	glPushMatrix();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBegin(GL_QUADS);
	glColor3f((float)135/255, (float)191/254, 1);
	glVertex2f(-len / 2, -len / 2);
	glVertex2f(len / 2, -len / 2);
	glVertex2f(len / 2, len / 2);
	glVertex2f(-len / 2, len / 2);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glPopMatrix();
}
void drawSpider() {
	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<360 ; i++) {
		glVertex2f(0.02 * cos(toradian(i)), 0.02 * sin(toradian(i)));
	}
	glEnd();
	glBegin(GL_LINES);
	//Leg1 right 1st
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,0.1f);
	glVertex2f(0.1f,0.1f);
	glVertex2f(0.13f,0.15f);

	//Leg2 right 2nd
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,0.04f);
	glVertex2f(0.1f,0.04f);
	glVertex2f(0.14f,0.09f);
	//Leg3 Right 3rd
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,-0.1f);
	glVertex2f(0.1f,-0.1f);
	glVertex2f(0.13f,-0.13f);
	//Leg4 Right 4th
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,-0.05f);
	glVertex2f(0.1f,-0.05f);
	glVertex2f(0.14f,-0.1f);

	//Left 1st leg
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,0.1f);
	glVertex2f(-0.1f,0.1f);
	glVertex2f(-0.13f,0.15f);

	//Leg2 right 2nd
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,0.04f);
	glVertex2f(-0.1f,0.04f);
	glVertex2f(-0.14f,0.09f);
	//Leg3 Right 3rd
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,-0.1f);
	glVertex2f(-0.1f,-0.1f);
	glVertex2f(-0.13f,-0.13f);
	//Leg4 Right 4th
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,-0.05f);
	glVertex2f(-0.1f,-0.05f);
	glVertex2f(-0.14f,-0.1f);
	glEnd();
}
void drawTriangle() {
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.139f, 0.090f, 0.043f);
	glVertex3f(-0.2f, -0.2f, 0.0f);
	glColor3f(0.139f, 0.090f, 0.043f);
	glVertex3f(-0.2f, 0.2f, 0.0f);
	glEnd();
}
void drawCanon(){
	glPushMatrix();
	glTranslatef(canon_x,canon_y,0);
	float x;
	if(mouse_x==0&&mouse_y==0)
		x=0;
	else
		x=atan((mouse_y)/(mouse_x))*180/Pi;
	if(x<0)
		x=180+x;
	//glVertex2f((mouse_x-windowWidth/2)*.005-canon_x,mouse_y*.001);
	//	cout<<w/2<<"!";
	//	cout<<x<<endl;
	//cout<<canon_y<<" "<<mouse_x<<" "<<mouse_y<<endl;
	glPushMatrix();
	glColor3f(0,0,0);
	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<180 ; i++) 
		glVertex2f(0.2 * cos(toradian(i)), 0.2 * sin(toradian(i)));
	glEnd();
	glBegin(GL_QUADS);
	glVertex2f(.2,0);
	glVertex2f(.2,.1);
	glVertex2f(.3,.1);
	glVertex2f(.3,0);
	glEnd();
	glBegin(GL_QUADS);
	glVertex2f(-.2,0);
	glVertex2f(-.2,.1);
	glVertex2f(-.3,.1);
	glVertex2f(-.3,0);
	glEnd();
	glRotatef(x,0,0,1);
	glBegin(GL_TRIANGLES);
	glVertex2f(.5,.05);
	glVertex2f(.6,0);
	glVertex2f(.5,-0.05);
	glEnd();
	glBegin(GL_QUADS);
	glVertex2f(0,.05);
	glVertex2f(.5,.05);
	glVertex2f(.5,-0.05);
	glVertex2f(0,-.05);
	glEnd();
	glPopMatrix();
	glPopMatrix();
}
void init() {
	glMatrixMode( GL_PROJECTION);
	glEnable(GL_DEPTH_TEST);        // Enable objects to be drawn ahead/behind one another
	glEnable(GL_COLOR_MATERIAL);    // Enable coloring
	//gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   // Setting a background color
}

void handleResize(int w1, int h1) {

	glViewport(0, 0, w1, h1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (float)w1 / (float)h1, 1.0f, 200.0f);
	//	gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	w=w1;
	h=h1;
	windowWidth = w * 2 / 3;
	windowHeight = h * 2 / 3;

}

void handleKeypress1(unsigned char key, int x, int y) {

	if (key == 27||key=='q') {
		exit(0);     // escape key is pressed
	}else if(key=='r')
		control=-1;
	else if(key=='g')
	{
		control=1;
	}else if(key=='b')
		control=0;
	else if(key=='p')
		pause^=1;

}

void handleKeypress2(int key, int x, int y) {

	if (key == GLUT_KEY_LEFT)
	{
		switch (control)
		{
			case 0:{
				       bool fl=1;
				       for(unsigned int i=0;i<sp_v.sz;i++)
				       {
					       if(sp_vel[i]==0&&fabs(sp_v[i].ff-(canon_x-.1))<=.1)
						       fl=0;
				       }
				       if(canon_x<=-box_len/2)
					       fl=0;
				       if(fl)canon_x -= 0.1;
				       break;
			       }
			case 1:{
				       bool fl=1;
				       for(unsigned int i=0;i<sp_v.sz;i++)
				       {
					       if(sp_vel[i]==0&&fabs(sp_v[i].ff-(rbox_x-.1))<=.1)
						       fl=0;
				       }
				       if(dragr)fl=1;
				       if(rbox_x<=-box_len/2)
					       fl=0;
				       if(fl)
					       rbox_x-=0.1;
				       break;}
			case -1:{  bool fl=1;
					for(unsigned int i=0;i<sp_v.sz;i++)
					{
						if(sp_vel[i]==0&&fabs(sp_v[i].ff-(lbox_x-.1))<=.1)
							fl=0;
					}
					if(dragl)fl=1;
					if(lbox_x<=-box_len/2)
						fl=0;
					if(fl)lbox_x-=0.1;


					break;}
		}
	}
	if (key == GLUT_KEY_RIGHT)
	{

		switch (control)
		{
			case 0:{
				       bool fl=1;
				       for(unsigned int i=0;i<sp_v.sz;i++)
				       {
					       if(sp_vel[i]==0&&fabs(sp_v[i].ff-(canon_x+.1))<=.1)
						       fl=0;
				       }
				       if(canon_x>=box_len/2)
					       fl=0;
				       if(fl)canon_x += 0.1;
				       break;
			       }
			case 1:{
				       bool fl=1;
				       for(unsigned int i=0;i<sp_v.sz;i++)
				       {
					       if(sp_vel[i]==0&&fabs(sp_v[i].ff-(rbox_x+.1))<=.1)
						       fl=0;
				       }
				       if(dragr)fl=1;
				       if(rbox_x>=box_len/2)
					       fl=0;
				       if(fl)
					       rbox_x+=0.1;
				       break;}
			case -1:{     bool fl=1;
					for(unsigned int i=0;i<sp_v.sz;i++)
					{
						if(sp_vel[i]==0&&fabs(sp_v[i].ff-(lbox_x+.1))<=.1)
							fl=0;
					}
					if(dragl)fl=1;
					if(lbox_x>=box_len/2)
						fl=0;
					if(fl)
						lbox_x+=0.1;


					break;}
		}

	}
	if (key == GLUT_KEY_UP)
		tri_y += 0.1;
	if (key == GLUT_KEY_DOWN)
		tri_y -= 0.1;
}
void* playshootsound(void* x){
	system("aplay LASER.WAV");
	return 0;
}
void handleMouseclick(int button, int state, int x, int y) {
	if (state == GLUT_DOWN)
	{
		if (button == GLUT_LEFT_BUTTON)
		{
			if(mouse2_x>=rbox_x-.2&&mouse2_x<=rbox_x+.2&&mouse2_y>=-box_len/2-.2&&mouse2_y<=-box_len/2+.2)
			{
				dragr=1;
				return;
			}
			if(mouse2_x>=lbox_x-.2&&mouse2_x<=lbox_x+.2&&mouse2_y>=-box_len/2-.2&&mouse2_y<=-box_len/2+.2)
			{
				dragl=1;
				return;
			}
			if(elapsed_time<100)
				return;
			elapsed_time=0;
			glMatrixMode(GL_PROJECTION);
			//glPushMatrix();
			//glTranslatef(0.0f,0.0f,0.0f);
			//glOrtho(0,w,0,h,-100,100);
			float x=atan((mouse_y)/(mouse_x));
			//glGetFloatv(GL_PROJECTION_MATRIX, projection);
			/*for(unsigned int tx=0;tx<10;tx++){
			  cout<<projection[tx]<<" ";
			  }
			  cout<<endl;*/
			//glPopMatrix();
			//glMatrixMode (GL_MODELVIEW); glLoadIdentity ();
			//gluPerspective(45.0f, (float)w / (float)h, 0.0f, 200.0f);
			//		cout<<(canon_x*w/h)*tan(45.0/2)<<endl;
			if(x<0)
				x+=Pi;
			laser_angle.pb(x*180/Pi);
			laser_vel.pb(mp(.05*cos(x),.05*sin(x)));
			laser_v.pb(mp(canon_x,canon_y));
			theta += 15;
			//		cout<<x*180/Pi<<": vels: "<<.05*cos(x)<<", "<<.05*sin(x)<<endl;
			pthread_t shootsnd;
			int scrap=0;
			pthread_create(&shootsnd,NULL,playshootsound,(void*)(&scrap));
		}
		else if (button == GLUT_RIGHT_BUTTON)
			theta -= 15;
	}else{
		dragr=0;
		dragl=0;
	}

}

int main(int argc, char **argv) {
	//sp_v.pb(mp(0.0f,0.0f));
	//sp_vel.pb((float)(minvel+(float)(rand()%(maxvel-minvel)))/1000);
	//Take world inputs
	cout<<"Enter square world side length: ";
	cin>>box_len;

	cout<<"Enter x position of left basket: ";
	cin>>lbox_x;
	cout<<"Enter x position of right basket: ";
	cin>>rbox_x;
	canon_y=-box_len/2;
	canon_x=0;
	// Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	w = glutGet(GLUT_SCREEN_WIDTH);
	h = glutGet(GLUT_SCREEN_HEIGHT);

	windowWidth = w * 2 / 3;
	windowHeight = h * 2 / 3;

	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

	glutCreateWindow("Assignment1");
	init();
	glutDisplayFunc(drawScene);
	glutMotionFunc( mouse1 );
	glutPassiveMotionFunc( mouse1 );
	glutIdleFunc(drawScene);
	glutKeyboardFunc(handleKeypress1);
	glutSpecialFunc(handleKeypress2);
	glutMouseFunc(handleMouseclick);
	glutReshapeFunc(handleResize);
	glutTimerFunc(10, update, 0);
	glutTimerFunc(5000, increasedif, 0);
	glutTimerFunc(1000, generate_spider, 0);

	glutMainLoop();
	return 0;
}
