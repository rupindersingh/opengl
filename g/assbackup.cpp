//Rupinder :D

#include<iostream>
#include<cmath>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
#include<GL/glut.h>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define toradian(x) (x*Pi/180)
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}

vector<pair<float,float> > sp_v;
vector<float> sp_vel,laser_angle;
vector<pair<float,float> > laser_v,laser_vel;
float box_len = 100.0f;
float tri_x = 0.0f;
float tri_y = 0.0f;
float theta = 0.0f; 
float mouse_x,mouse_y;
float canon_x = 0.0f; 
float canon_y = -box_len/2; 
float canon_rot = 0.0f; 
int minvel=10;
int maxvel=50;
 	float windowWidth;
	float windowHeight;
float w,h;
void drawSpider(void);
void drawBox(float);
void drawCanon(void);
void mouse1(int x, int y)
{

	mouse_x=float(x);
	mouse_y=h-(float)y;

}
void drawlaser(int angle)
{
	glBegin(GL_LINES);
	glColor3f((float)(rand()%1000)/1000,(float)(rand()%1000)/1000,(float)(rand()%1000)/1000);
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.4*cos(angle*Pi/180),.4*sin(angle*Pi/180));
	glEnd();
}
	
void drawScene() {
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();

	// Draw Box
	glTranslatef(0.0f, 0.0f, -5.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	drawBox(box_len);
	drawCanon();
	// Draw Spiders
	unsigned int i;
	for(i=0;i<sp_v.size();i++)
	{
		glPushMatrix();
		glTranslatef(sp_v[i].ff, sp_v[i].ss, 0.0f);
		glColor3f(0.0f, 1.0f, 0.0f);
		drawSpider();
		glPopMatrix();
	}
	//Draw Lasers
	for(i=0;i<laser_v.size();i++)
	{
		glPushMatrix();
		glTranslatef(laser_v[i].ff, laser_v[i].ss, 0.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		drawlaser(laser_angle[i]);
		glPopMatrix();
	}
	glPopMatrix();
	glutSwapBuffers();
}
void update(int value) {
	unsigned int i,j;
	for(i=0;i<sp_v.sz;i++)
	{
		sp_v[i].ss-=sp_vel[i];
		if(sp_v[i].ss<-box_len/2){
			sp_v.erase(sp_v.begin()+i);
			sp_vel.erase(sp_vel.begin()+i);
		}
	}
	for(i=0;i<laser_v.sz;i++)
	{
		laser_v[i].ss+=laser_vel[i].ss;
		laser_v[i].ff+=laser_vel[i].ff;
		if(laser_v[i].ff<-box_len/2||laser_v[i].ff>box_len/2||laser_v[i].ss<-box_len/2||laser_v[i].ss>box_len/2)
		{
			laser_v.erase(laser_v.begin()+i);
			laser_vel.erase(laser_vel.begin()+i);
			laser_angle.erase(laser_angle.begin()+i);
		}
	}
	for(i=0;i<sp_v.sz;i++)
	{
		for(j=0;j<laser_v.sz;j++)
		{
		}
	}
	//cout<<mouse_x<<" "<<mouse_y<<endl;
	glutTimerFunc(10, update, 0);
}
void generate_spider(int scrap)
{
	sp_v.pb(mp(-box_len/2+box_len*((float)(rand()%1000)/1000),box_len/2));
	//sp_v.pb(mp(0.0f,0.0f));
	sp_vel.pb((float)(minvel+(float)(rand()%(maxvel-minvel)))/1000);
	glutTimerFunc(2000, generate_spider,0);
}
void drawBox(float len) {

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_QUADS);
	glVertex2f(-len / 2, -len / 2);
	glVertex2f(len / 2, -len / 2);
	glVertex2f(len / 2, len / 2);
	glVertex2f(-len / 2, len / 2);
	glEnd();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}
void drawSpider() {

	glBegin(GL_TRIANGLE_FAN);
	for(int i=0 ; i<360 ; i++) {
		glVertex2f(0.02 * cos(toradian(i)), 0.02 * sin(toradian(i)));
	}
	glEnd();
	glBegin(GL_LINES);
	//Leg1 right 1st
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,0.1f);
	glVertex2f(0.1f,0.1f);
	glVertex2f(0.13f,0.15f);

	//Leg2 right 2nd
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,0.04f);
	glVertex2f(0.1f,0.04f);
	glVertex2f(0.14f,0.09f);
	//Leg3 Right 3rd
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,-0.1f);
	glVertex2f(0.1f,-0.1f);
	glVertex2f(0.13f,-0.13f);
	//Leg4 Right 4th
	glVertex2f(0.0f,0.0f);
	glVertex2f(0.1f,-0.05f);
	glVertex2f(0.1f,-0.05f);
	glVertex2f(0.14f,-0.1f);
	
	//Left 1st leg
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,0.1f);
	glVertex2f(-0.1f,0.1f);
	glVertex2f(-0.13f,0.15f);

	//Leg2 right 2nd
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,0.04f);
	glVertex2f(-0.1f,0.04f);
	glVertex2f(-0.14f,0.09f);
	//Leg3 Right 3rd
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,-0.1f);
	glVertex2f(-0.1f,-0.1f);
	glVertex2f(-0.13f,-0.13f);
	//Leg4 Right 4th
	glVertex2f(0.0f,0.0f);
	glVertex2f(-0.1f,-0.05f);
	glVertex2f(-0.1f,-0.05f);
	glVertex2f(-0.14f,-0.1f);
	glEnd();
}
void drawTriangle() {
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.2f, -0.2f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glEnd();
}
void drawCanon(){
	glPushMatrix();
	glTranslatef(canon_x,canon_y,0.0);
	float x=atan(mouse_y/(mouse_x-w/2-canon_x))*180/Pi;
	if(x<0)
		x=180+x;
	//glVertex2f((mouse_x-windowWidth/2)*.005-canon_x,mouse_y*.001);
//	cout<<w/2<<"!";
	//cout<<x<<endl;
	glRotatef(x,0,0,1);
	drawTriangle();
	glPopMatrix();
}
void init() {
	glMatrixMode( GL_PROJECTION);
	glEnable(GL_DEPTH_TEST);        // Enable objects to be drawn ahead/behind one another
	glEnable(GL_COLOR_MATERIAL);    // Enable coloring
	//gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   // Setting a background color
}

void handleResize(int w1, int h1) {

	glViewport(0, 0, w1, h1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
//	gluPerspective(45.0f, (float)w1 / (float)h1, 0.0f, 200.0f);
	gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	w=w1;
	h=h1;
 	windowWidth = w * 2 / 3;
	windowHeight = h * 2 / 3;

}

void handleKeypress1(unsigned char key, int x, int y) {

	if (key == 27) {
		exit(0);     // escape key is pressed
	}
}

void handleKeypress2(int key, int x, int y) {

	if (key == GLUT_KEY_LEFT)
		canon_x -= 0.1;
	if (key == GLUT_KEY_RIGHT)
		canon_x += 0.1;
	if (key == GLUT_KEY_UP)
		tri_y += 0.1;
	if (key == GLUT_KEY_DOWN)
		tri_y -= 0.1;
}

void handleMouseclick(int button, int state, int x, int y) {

	if (state == GLUT_DOWN)
	{
		if (button == GLUT_LEFT_BUTTON)
		{
			float x=atan(mouse_y/(mouse_x-w/2-((canon_x*h/w)*tan(45.0/2))));
			cout<<(canon_x*w/h)*tan(45.0/2)<<endl;
			if(x<0)
				x+=Pi;
			laser_angle.pb(x*180/Pi);
			laser_vel.pb(mp(.05*cos(x),.05*sin(x)));
			laser_v.pb(mp(canon_x,canon_y));
			theta += 15;
			cout<<x*180/Pi<<": vels: "<<.05*cos(x)<<", "<<.05*sin(x)<<endl;
		}
		else if (button == GLUT_RIGHT_BUTTON)
			theta -= 15;
	}
}

int main(int argc, char **argv) {
	sp_v.pb(mp(0.0f,0.0f));
	sp_vel.pb((float)(minvel+(float)(rand()%(maxvel-minvel)))/1000);

	// Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	w = glutGet(GLUT_SCREEN_WIDTH);
	h = glutGet(GLUT_SCREEN_HEIGHT);

 	windowWidth = w * 2 / 3;
	windowHeight = h * 2 / 3;

	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

	glutCreateWindow("Assignment1");
	init();
	glutDisplayFunc(drawScene);
	glutPassiveMotionFunc( mouse1 );
	glutIdleFunc(drawScene);
	glutKeyboardFunc(handleKeypress1);
	glutSpecialFunc(handleKeypress2);
	glutMouseFunc(handleMouseclick);
	glutReshapeFunc(handleResize);
	glutTimerFunc(10, update, 0);
	glutTimerFunc(1000, generate_spider, 0);

	glutMainLoop();
	return 0;
}
