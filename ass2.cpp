//Rupinder :D

#include<iostream>
#include<cmath>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
#include<GL/glut.h>
#include<pthread.h>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
ytypedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define toradian(x) (x*Pi/180)
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}
float windowWidth,windowHeight,w,h,camera_x=0,camera_y=0,camera_z=0;
GLfloat ctrlpoints[4][3] = {
	{ -4.0, -4.0, 0.0}, { -2.0, 4.0, 0.0}, 
	{2.0, -4.0, 0.0}, {4.0, 4.0, 0.0}};


void GetOGLPos(float x, float y,GLdouble* resx,GLdouble* resy)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;
	glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
	glGetDoublev( GL_PROJECTION_MATRIX, projection );
	glGetIntegerv( GL_VIEWPORT, viewport );
	winX = (float)x;
	winY = (float)viewport[3] - (float)y;
	glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

	gluUnProject( winX, winY, 0.0f, modelview, projection, viewport, &posX, &posY, &posZ);
	*resx=posX;
	*resy=posY;
	return;
}
void mouse1(int x, int y)
{

	glPopMatrix();
}
void drawCylinder(int numMajor, int numMinor, float height, float radius)
{
	double majorStep = height / numMajor;
	double minorStep = 2.0 * M_PI / numMinor;
	int i, j;

	for (i = 0; i < numMajor; ++i) {
		float z0 = 0.5 * height - i * majorStep;
		float z1 = z0 - majorStep;

		glBegin(GL_TRIANGLE_STRIP);
		for (j = 0; j <= numMinor; ++j) {
			double a = j * minorStep;
			float x = radius * cos(a);
			float y = radius * sin(a);
			glNormal3f(x / radius, y / radius, 0.0);
			glTexCoord2f(j / numMinor, i / numMajor);
			glVertex3f(x, y, z0);

			glTexCoord2f(j / numMinor, (i + 1) / numMajor);
			glVertex3f(x, y, z1);
		}
		glEnd();
	}
}

void drawScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(camera_x,camera_y,camera_z,0,0,-5,0,1,0);
	glPushMatrix();
	glTranslatef(0,0,-5);
	glutSolidSphere(0.75,20,20);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(5,0,-5);
	glutSolidSphere(0.75,20,20);
	glPopMatrix();
	glutSwapBuffers();
}

void drawTriangle() {
	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glColor3f(0.139f, 0.090f, 0.043f);
	glVertex3f(-0.2f, -0.2f, 0.0f);
	glColor3f(0.139f, 0.090f, 0.043f);
	glVertex3f(-0.2f, 0.2f, 0.0f);
	glEnd();
}

void init() {
	glMatrixMode( GL_PROJECTION);
	glEnable(GL_DEPTH_TEST);        // Enable objects to be drawn ahead/behind one another
	glEnable(GL_COLOR_MATERIAL);    // Enable coloring
	//gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   // Setting a background color
	glShadeModel(GL_FLAT);
	glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, 4, &ctrlpoints[0][0]);
	glEnable(GL_MAP1_VERTEX_3);
}

void handleResize(int w1, int h1) {

	glViewport(0, 0, w1, h1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(160.0f, (float)w1 / (float)h1, 1.0f, 200.0f);
	//	gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	w=w1;
	h=h1;
	windowWidth = w * 2 / 3;
	windowHeight = h * 2 / 3;

}

void handleKeypress1(unsigned char key, int x, int y) {

	if (key == 27||key=='q') {
		exit(0);     // escape key is pressed
	}
}

void handleKeypress2(int key, int x, int y) {

	if (key == GLUT_KEY_UP);
	if (key == GLUT_KEY_DOWN);
}

void handleMouseclick(int button, int state, int x, int y) {
	if (state == GLUT_DOWN)
	{
		if (button == GLUT_LEFT_BUTTON)
		{
		}
	}

}
void update(int value)
{
	camera_z-=.01;
	glutTimerFunc(10, update, 0);
}
int main(int argc, char **argv) {
	// Initialize GLUT
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	w = glutGet(GLUT_SCREEN_WIDTH);
	h = glutGet(GLUT_SCREEN_HEIGHT);
	windowWidth = w * 2 / 3;
	windowHeight = h * 2 / 3;

	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

	glutCreateWindow("Assignment2");
	init();
	glutDisplayFunc(drawScene);
	glutMotionFunc( mouse1 );
	glutPassiveMotionFunc( mouse1 );
	glutIdleFunc(drawScene);
	glutKeyboardFunc(handleKeypress1);
	glutSpecialFunc(handleKeypress2);
	glutMouseFunc(handleMouseclick);
	glutReshapeFunc(handleResize);
	glutTimerFunc(10, update, 0);

	glutMainLoop();
	return 0;
}
