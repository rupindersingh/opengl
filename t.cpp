#include<GL/glut.h>
#include<iostream>
using namespace std;

GLint viewWidth = 400, viewHeight = 400;
int centerX = 200, centerY = 200;
int tempX, tempY;

void drawLine( int x, int y)
{
	glBegin( GL_LINES);
	glVertex2i( centerX, centerY);
	glVertex2i( x, y);
	glVertex2i(0,0);
	glVertex2i(100,100);
	glEnd();
}

void init()
{
	glClearColor( 0.0, 0.0, 1.0, 1.0);
	glMatrixMode( GL_PROJECTION);
	gluOrtho2D( 0.0, 400.0, 0.0, 400.0);
}

void myDisplay()
{
	glClear( GL_COLOR_BUFFER_BIT);
	glColor3f( 0.0, 0.0, 0.0);
cout<<tempX<<endl;
	drawLine( tempX, tempY);

	glutSwapBuffers();
}

void myMouseMove( int x, int y)
{
	tempX = x;
	tempY = viewHeight - y;

	glutPostRedisplay();
}

int main( int argc, char ** argv)
{
	glutInit( &argc, argv);
	glutInitDisplayMode( GLUT_DOUBLE| GLUT_RGB);
	glutInitWindowPosition( 100, 100);
	glutInitWindowSize( viewWidth, viewHeight);
	glutCreateWindow( "problem demonstration");

	init();
	glutDisplayFunc( myDisplay);
	glutPassiveMotionFunc( myMouseMove);

	glutMainLoop();
	return 0;
}
