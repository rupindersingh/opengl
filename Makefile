CC = g++
CFLAGS = -Wall
PROG = f.out

SRCS = ass1.cpp
LIBS = -lglut -lGL -lGLU -lpthread 

all: $(PROG)

$(PROG):	$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(LIBS)

clean:
	rm -f $(PROG)
