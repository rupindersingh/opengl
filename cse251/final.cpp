//Rupinder :D

#include<iostream>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}


int main(int argc, char **argv) {
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

		int w = glutGet(GLUT_SCREEN_WIDTH);
		int h = glutGet(GLUT_SCREEN_HEIGHT);
		int windowWidth = w * 2 / 3;
		int windowHeight = h * 2 / 3;

		glutInitWindowSize(windowWidth, windowHeight);
		glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

		glutCreateWindow("CSE251_sampleCode");
		initRendering();

		glutDisplayFunc(drawScene);
		glutIdleFunc(drawScene);
		glutKeyboardFunc(handleKeypress1);
		glutSpecialFunc(handleKeypress2);
		glutMouseFunc(handleMouseclick);
		glutReshapeFunc(handleResize);
		glutTimerFunc(10, update, 0);

		glutMainLoop();
	return 0;
}
