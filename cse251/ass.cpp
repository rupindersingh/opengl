//Rupinder :D

#include<iostream>
#include<map>
#include<set>
#include<string.h>
#include<stdio.h>
#include<vector>
#include<math.h>
#include<string>
#include<algorithm>
#include<iterator>
#include<iomanip>
#include<limits.h>
#include<numeric>
#include<GL/glut.h>
using namespace std;

#define Pi 3.14159265358979323846264338327950288419716939937510582
typedef long long int lld;
typedef vector<int> vi;
typedef pair<int,int> pii;
#define sz size()
#define MOD 1000000007
#define pb push_back
#define mp make_pair
#define pf push_front
#define ppb pop_back
#define ff first
#define ss second
#define rep(i,n) for(i=0;i<n;i++)
#define all(a) a.begin(),a.end()
lld modpow(lld a,lld n,lld temp){lld res=1,y=a;while(n>0){if(n&1)res=(res*y)%temp;y=(y*y)%temp;n/=2;}return res%temp;}
lld gcd(lld a,lld b){if(a==0)return(b);else return(gcd(b%a,a));}
int z=5.0f;
void init()
{
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);
}
void handleResize(int w,int h){
	glViewport(0,0,w,h);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluPerspective(45.0,(double)w/(double)h,1.0,200.0);
}
void drawtriangle()
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glPushMatrix();
	glTranslatef(0.0f,0.0f,-z);
	glColor3f(0.5f,0.0f,0.8f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f,0.0f,0.0);
	glColor3f(0.5f,0.0f,0.0f);
	glVertex3f(0.0f, 1.0f, 0.0);
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(1.0f,0.0f,0.0);
	glEnd();
	glPopMatrix();
	glutSwapBuffers();
}
int main(int argc,char ** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowSize(400,400);

	glutCreateWindow("Hello World!!");
	init();
	glutDisplayFunc(drawtriangle);
	glutReshapeFunc(handleResize);
	glutMainLoop();
	return 0;
}
