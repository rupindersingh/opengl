

#include <iostream>
#include <stdlib.h>
#define Pi 3.14159265358979323846264338327950288419716939937510582

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include<cmath>
#include "imageloader.h"
#include "vec3f.h"
#include<vector>
#include<stdio.h>
#define sz size()
#define toradian(x) 180*x/Pi
using namespace std;
float bike_x=29,bike_y=0,bike_z=0,bike_angle=0,bike_xspeed=0,bike_yspeed=0,bike_zspeed=0,bike_xa=0,bike_ya=0,bike_za=0,camera_x,camera_y,camera_z,view=3,g=.04,bike_speed=0,bike_a=0,tilt=0,score=0,mouse_x,mouse_y,w,h,broke=0,handle_rot=0,rotr=0,rotl=0,play=0,view4dist=20;
vector<pair<float,float> > foss;
//Represents a terrain, by storing a set of heights and normals at 2D locations

void mouse2(int button, int state, int x, int y)
{
	//cout<<button<<endl;
	// Wheel reports as button 3(scroll up) and button 4(scroll down)
		//cout<<view4dist<<endl;
		if(view==4)
		{
	if ((button == 3)) // It's a wheel event
	{
		view4dist-=2;
		//cout<<"done "<<view4dist<<endl;
	}
	if ((button == 4)) // It's a wheel event
		view4dist+=2;
		}
	 glutPostRedisplay();
}
void GetOGLPos(float x, float y,GLdouble* resx,GLdouble* resy)
{
	GLint viewport[4];
	GLdouble modelview[16];
	GLdouble projection[16];
	GLfloat winX, winY, winZ;
	GLdouble posX, posY, posZ;
	glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
	glGetDoublev( GL_PROJECTION_MATRIX, projection );
	glGetIntegerv( GL_VIEWPORT, viewport );
	winX = (float)x;
	winY = (float)viewport[3] - (float)y;
	glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

	gluUnProject( winX, winY, 0.0f, modelview, projection, viewport, &posX, &posY, &posZ);
	*resx=posX;
	*resy=posY;
	return;
}
void mouse1(int x, int y)
{

	mouse_x=float(x);
	mouse_y=(float)y;
	glPushMatrix();
	GLdouble t_x,t_y;
	GetOGLPos(mouse_x,mouse_y,&t_x,&t_y);
	GetOGLPos(mouse_x,mouse_y,&t_x,&t_y);
	//	mouse_x=t_x;
	//	mouse_y=t_y;
	glPopMatrix();
	//	cout<<mouse_x<<" "<<mouse_y<<" "<<w<<" "<<endl;
}

class Terrain {
	private:
		int w; //Width
		int l; //Length
		float** hs; //Heights
		Vec3f** normals;
		bool computedNormals; //Whether normals is up-to-date
	public:
		Terrain(int w2, int l2) {
			w = w2;
			l = l2;

			hs = new float*[l];
			for(int i = 0; i < l; i++) {
				hs[i] = new float[w];
			}

			normals = new Vec3f*[l];
			for(int i = 0; i < l; i++) {
				normals[i] = new Vec3f[w];
			}

			computedNormals = false;
		}

		~Terrain() {
			for(int i = 0; i < l; i++) {
				delete[] hs[i];
			}
			delete[] hs;

			for(int i = 0; i < l; i++) {
				delete[] normals[i];
			}
			delete[] normals;
		}

		int width() {
			return w;
		}

		int length() {
			return l;
		}

		//Sets the height at (x, z) to y
		void setHeight(int x, int z, float y) {
			hs[z][x] = y;
			computedNormals = false;
		}

		//Returns the height at (x, z)
		float getHeight(int x, int z) {
			return hs[z][x];
		}

		//Computes the normals, if they haven't been computed yet
		void computeNormals() {
			if (computedNormals) {
				return;
			}

			//Compute the rough version of the normals
			Vec3f** normals2 = new Vec3f*[l];
			for(int i = 0; i < l; i++) {
				normals2[i] = new Vec3f[w];
			}

			for(int z = 0; z < l; z++) {
				for(int x = 0; x < w; x++) {
					Vec3f sum(0.0f, 0.0f, 0.0f);

					Vec3f out;
					if (z > 0) {
						out = Vec3f(0.0f, hs[z - 1][x] - hs[z][x], -1.0f);
					}
					Vec3f in;
					if (z < l - 1) {
						in = Vec3f(0.0f, hs[z + 1][x] - hs[z][x], 1.0f);
					}
					Vec3f left;
					if (x > 0) {
						left = Vec3f(-1.0f, hs[z][x - 1] - hs[z][x], 0.0f);
					}
					Vec3f right;
					if (x < w - 1) {
						right = Vec3f(1.0f, hs[z][x + 1] - hs[z][x], 0.0f);
					}

					if (x > 0 && z > 0) {
						sum += out.cross(left).normalize();
					}
					if (x > 0 && z < l - 1) {
						sum += left.cross(in).normalize();
					}
					if (x < w - 1 && z < l - 1) {
						sum += in.cross(right).normalize();
					}
					if (x < w - 1 && z > 0) {
						sum += right.cross(out).normalize();
					}

					normals2[z][x] = sum;
				}
			}

			//Smooth out the normals
			const float FALLOUT_RATIO = 0.1f;
			for(int z = 0; z < l; z++) {
				for(int x = 0; x < w; x++) {
					Vec3f sum = normals2[z][x];

					if (x > 0) {
						sum += normals2[z][x - 1] * FALLOUT_RATIO;
					}
					if (x < w - 1) {
						sum += normals2[z][x + 1] * FALLOUT_RATIO;
					}
					if (z > 0) {
						sum += normals2[z - 1][x] * FALLOUT_RATIO;
					}
					if (z < l - 1) {
						sum += normals2[z + 1][x] * FALLOUT_RATIO;
					}

					if (sum.magnitude() == 0) {
						sum = Vec3f(0.0f, 1.0f, 0.0f);
					}
					normals[z][x] = sum;
				}
			}

			for(int i = 0; i < l; i++) {
				delete[] normals2[i];
			}
			delete[] normals2;

			computedNormals = true;
		}

		//Returns the normal at (x, z)
		Vec3f getNormal(int x, int z) {
			if (!computedNormals) {
				computeNormals();
			}
			return normals[z][x];
		}
};

//Loads a terrain from a heightmap.  The heights of the terrain range from
//-height / 2 to height / 2.
Terrain* loadTerrain(const char* filename, float height) {
	Image* image = loadBMP(filename);
	Terrain* t = new Terrain(image->width, image->height);
	for(int y = 0; y < image->height; y++) {
		for(int x = 0; x < image->width; x++) {
			unsigned char color =
				(unsigned char)image->pixels[3 * (y * image->width + x)];
			float h = height * ((color / 255.0f) - 0.5f);
			t->setHeight(x, y, h);
		}
	}

	delete image;
	t->computeNormals();
	return t;
}

float _angle = 60.0f;
Terrain* _terrain;

void cleanup() {
	delete _terrain;
}

void handleKeypress(unsigned char key, int x, int y) {
	switch (key) {
		case 27: //Escape key
			cleanup();
			exit(0);
	}
}

void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	glShadeModel(GL_SMOOTH);
}

void handleResize(int w1, int h1) {
	glViewport(0, 0, w1, h1);
	w=w1;h=h1;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (double)w1 / (double)h1, 1.0, 200.0);
}
void drawfossils(void)
{
	for(unsigned int i=0;i<foss.sz;i++)
	{
		glPushMatrix();
		glColor3f((float)152/255,(float)161/255,(float)195/255);
		glTranslatef(foss[i].first,3+_terrain->getHeight(foss[i].first+_terrain->width()/2,foss[i].second+_terrain->length()/2),foss[i].second);
		glutSolidTeapot(1);
		glPopMatrix();
	}
	return;
}
void drawbike()
{
	//float scale = 5.0f / max(_terrain->width() - 1, _terrain->length() - 1);
	glPushMatrix();
	//	glLoadIdentity();
	glTranslatef(bike_x,bike_y+1,bike_z);
	Vec3f normal = _terrain->getNormal(bike_x+_terrain->width()/2, bike_z+_terrain->length()/2);
	//float u=sqrt(normal[0]*normal[0]+normal[1]*normal[1]+normal[2]*normal[2]);
	glColor3f(1,0,0);
	/*	glBegin(GL_QUADS);
		glVertex3f(-.2,0,0);
		glVertex3f(.2,0,0);
		glVertex3f(.2+normal[0],normal[1],normal[2]);
		glVertex3f(-.2+normal[0],normal[1],normal[2]);
		glEnd();*/
	glColor3f(0,0,1);
	//glutSolidSphere(2,20,20);

	//cout<<theta<<endl;
	glPushMatrix();
	glRotatef(-atan(normal[0]/normal[1])*180/Pi,0,0,1);
	glRotatef(atan(normal[2]/normal[1])*180/Pi,1,0,0);
	glRotatef(-bike_angle*180/Pi,0,1,0);
	glRotatef(tilt,1,0,0);

	/*	glBegin(GL_QUADS);
		glVertex3f(-1,0,0);
		glVertex3f(1,0,0);
		glVertex3f(1,5,0);
		glVertex3f(-1,5,0);*/
	glEnd();
	glPushMatrix();
	glTranslatef(-1,1,0);
	//Back
	glutSolidCube(1.5);
	//front
	glPopMatrix();
	glBegin(GL_QUADS);
	glVertex3f(1,0,-1);
	glVertex3f(1,3,-1);
	glVertex3f(1,3,1);
	glVertex3f(1,0,1);
	glEnd();
	//back Tyre
	glPushMatrix();
	glTranslatef(-1,0,0);
	glColor3f(0,0,0);
	glutSolidCone(1,.5,100,100);
	glutSolidCone(1,-.5,100,100);
	glPopMatrix();
	//stipeny
	glPushMatrix();
	glTranslatef(-2,1.5,0);
	glRotatef(90,0,1,0);
	glColor3f(0,0,0);
	glutSolidCone(1,.5,100,100);
	glutSolidCone(1,-.5,100,100);
	glPopMatrix();

	glPushMatrix();
	glRotatef(handle_rot,0,1,0);
	//front Tyre
	glPushMatrix();
	glTranslatef(1,0,0);
	glColor3f(0,0,0);
	glutSolidCone(1,.5,100,100);
	glutSolidCone(1,-.5,100,100);
	glPopMatrix();
	//headlight
	glPushMatrix();
	glColor3f(0,0,255);
	glTranslatef(1,3,0);
	glutSolidSphere(.5,100,100);
	glTranslatef(.2,0,0);
	glColor3f(255,255,0);
	glutSolidSphere(.5,100,100);
	glPopMatrix();
	//handle
	glPushMatrix();
	glColor3f(0,0,0);
	glTranslatef(1,3,-2);
	gluCylinder(gluNewQuadric(),.1,.1,4,100,100);
	glPopMatrix();
	glPopMatrix();

	//Base
	glColor3f(0,0,255);
	glBegin(GL_QUADS);
	glVertex3f(1,0,-1);
	glVertex3f(1,0,1);
	glVertex3f(-1,0,1);
	glVertex3f(-1,0,-1);
	glEnd();
	//		glRotatef(atan(normal[2]/normal[1])*180/Pi,1,0,0);
	//		glRotatef(atan(normal[1]/normal[0])*180/Pi,0,0,1);
	glPopMatrix();
	glPopMatrix();
}
void drawstream()
{
	glPushMatrix();
	glTranslatef(10,-6,30);
	glColor3f(0,0,250);
	glBegin(GL_QUADS);
	glVertex3f(-5,0,30);
	glVertex3f(-5,0,-30);
	glVertex3f(5,0,-30);
	glVertex3f(5,0,30);
	glEnd();
	glPopMatrix();
}
void drawScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glColor3f(3.0/255,13.0/255,100.0/255);
	glutSolidSphere(170,200,200);
	float mag=sqrt(bike_xspeed*bike_xspeed+bike_zspeed*bike_zspeed);
	if(view==1)
		gluLookAt(camera_x,camera_y,camera_z,bike_x+20*bike_xspeed/mag,bike_y+5,bike_z+20*bike_zspeed/mag,0,1,0);
	else if(view==2)
		gluLookAt(camera_x,camera_y,camera_z,bike_x+20*bike_xspeed/mag,bike_y+5,bike_z+20*bike_zspeed/mag,0,1,0);
	else if(view==3)
		gluLookAt(camera_x,camera_y,camera_z,bike_x,bike_y,bike_z,0,1,0);
	else if(view==4)
		gluLookAt(camera_x,camera_y,camera_z,bike_x,bike_y,bike_z,0,1,0);
	else if(view==5)
		gluLookAt(camera_x,camera_y,camera_z,bike_x,bike_y,bike_z,0,1,0);
	GLfloat ambientColor[] = {0.4f, 0.4f, 0.4f, 1.0f};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);

	GLfloat lightColor0[] = {0.3f, 0.3f, 0.3f, 1.0f};
	GLfloat lightPos0[] = {-0.5f, 0.3f, 0.1f, 0.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);
	float theta;
	if(bike_xspeed==0&&bike_zspeed==0)
		theta=0;
	else
	{
		if(bike_xspeed>=0)
			theta=atan(bike_zspeed/bike_xspeed);
		else
			theta=Pi-atan(bike_zspeed/-bike_xspeed);
	}
	GLfloat lightColor1[] = {.4f, .4f, 0.0f, .4f};
	GLfloat lightatten1[] = {.005f, .005f, 0.005f};
	GLfloat lightPos1[] = {bike_x,bike_y+2, bike_z, 1.0f};
	GLfloat spotPos1[] = {10*cos(theta), -0.1f, 10*sin(theta)};
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor1);
	glLightfv(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, lightatten1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightColor1);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);
	glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0);
	glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spotPos1);

	//	float scale = 5.0f / max(_terrain->width() - 1, _terrain->length() - 1);
	//glScalef(scale, scale, scale);
	glPushMatrix();
	glTranslatef(-(float)(_terrain->width() - 1) / 2,
			0.0f,
			-(float)(_terrain->length() - 1) / 2);

	glColor3f(0.3f, 0.9f, 0.0f);
	for(int z = 0; z < _terrain->length() - 1; z++) {
		//Makes OpenGL draw a triangle at every three consecutive vertices
		glBegin(GL_TRIANGLE_STRIP);
		for(int x = 0; x < _terrain->width(); x++) {
			if(_terrain->getHeight(x,z)<-7)
				glColor3f(0.0f,0.0f,0.9f);
			else if(fabs(x-bike_x-_terrain->width()/2)<1&&fabs(z-bike_z-_terrain->length()/2)<1)
			{
				glColor3f(0.2f, 0.6f, 0.0f);
			}else
				glColor3f(0.3f, 0.9f, 0.0f);
			Vec3f normal = _terrain->getNormal(x, z);
			glNormal3f(normal[0], normal[1], normal[2]);
			glVertex3f(x, _terrain->getHeight(x, z), z);
			normal = _terrain->getNormal(x, z + 1);
			glNormal3f(normal[0], normal[1], normal[2]);
			glVertex3f(x, _terrain->getHeight(x, z + 1), z + 1);
		}
		glEnd();
	}
	//	cout<<bikebike_zendl;
	glPopMatrix();
	drawbike();
	drawfossils();
	//	drawfossils();
	//	drawstream();
	//	glDisable( GL_DEPTH_TEST );
	glColor3f(0.0f, 0.0f, 0.0f);
	glRasterPos2f(0.1,0.5);
	char buf[300];
	sprintf( buf,"Your  score: %.0f", score ) ;
	const char * p = buf ;
	do glutBitmapCharacter( GLUT_BITMAP_HELVETICA_18, *p ); while( *(++p) ) ;

	glutSwapBuffers();
}
void reset(int val)
{
	tilt=0;
	bike_x=0;
	bike_z=0;
}
void update(int value){
	if(broke)
	{
		glutTimerFunc(1000, reset, 0);
		broke=0;
	}
	Vec3f normal = _terrain->getNormal(bike_x+_terrain->width()/2, bike_z+_terrain->length()/2);
	//vertical angle with normal
	float vangle=atan(sqrt(normal[0]*normal[0]+normal[2]*normal[2])/normal[1])*180/Pi;
	//cout<<tilt<<" "<<-tilt+vangle<<endl;
	if(vangle+abs(tilt)>30)
	{
		score=0;
		bike_xa=0;
		bike_za=0;
		bike_xspeed=0;
		bike_zspeed=0;
		tilt=90;
		broke=1;
	}

	for(unsigned int i=0;i<foss.sz;i++)
	{
		if(sqrt((bike_x-foss[i].first)*(bike_x-foss[i].first)+(bike_z-foss[i].second)*(bike_z-foss[i].second))<4)
		{
			score++;
			foss.erase(foss.begin()+i);
		}
	}
	if(bike_xspeed==0&&bike_zspeed==0);
	else
	{
		if(bike_xspeed>=0)
			bike_angle=atan(bike_zspeed/bike_xspeed);
		else
			bike_angle=Pi-atan(bike_zspeed/-bike_xspeed);
	}
	if(view==3){
		camera_x=bike_x+25;
		camera_y=bike_y+25;
		camera_z=bike_z+25;
	}else if(view==2)
	{
		camera_x=bike_x-5*cos(bike_angle);
		camera_y=bike_y+8;
		camera_z=bike_z-5*sin(bike_angle);
	}else if(view==1)
	{
		float theta;
		if(bike_xspeed==0&&bike_zspeed==0)
			theta=0;
		else
		{
			if(bike_xspeed>=0)
				theta=atan(bike_zspeed/bike_xspeed);
			else
				theta=Pi-atan(bike_zspeed/-bike_xspeed);
		}
		//cout<<bike_xspeed<<" ";
		//cout<<bike_zspeed<<endl;
		camera_x=bike_x+10*cos(bike_angle);
		camera_y=bike_y+10;
		camera_z=bike_z+10*sin(bike_angle);
	}else if(view==5)
	{
		camera_x=bike_x-20*cos(bike_angle);
		camera_y=bike_y+10;
		camera_z=bike_z-20*sin(bike_angle);
	}else if(view==4)
	{
		//cout<<mouse_x/w<<endl;
		float temptheta=10*(mouse_x/w);
//		cout<<view4dist<<endl;
		camera_x=bike_x-view4dist*cos(temptheta);
		camera_y=bike_y+view4dist/2;
		camera_z=bike_z-view4dist*sin(temptheta);
	}
	_angle += 1.0f;
	if (_angle > 360) {
		_angle -= 360;
	}
	//vertical acceleration
	float thetax,thetaz;
	if(normal[1]>=0)
		thetax=atan(normal[0]/normal[1]);
	else
		thetax=Pi-atan(normal[0]/-normal[1]);
	if(normal[1]>=0)
		thetaz=atan(normal[2]/normal[1]);
	else
		thetaz=Pi-atan(normal[2]/-normal[1]);
	bike_ya=sqrt(bike_xa*bike_xa*tan(thetax)*tan(thetax)+bike_za*bike_za*tan(thetaz)*tan(thetaz));
	//cout<<bike_ya<<endl;
	//bike_y=_terrain->getHeight(bike_x+_terrain->width()/2,bike_z+_terrain->length()/2);
	float temp;
	//	cout<<temp<<endl;
	if(bike_x+bike_xspeed<(_terrain->length())/2&&bike_x+bike_xspeed>-_terrain->length()/2){
		temp=_terrain->getHeight(bike_x+bike_xspeed+_terrain->width()/2,bike_z+_terrain->length()/2);
		if(temp>-7){
			bike_x+=bike_xspeed;
			bike_xspeed+=bike_xa;
		}else{
			bike_xa=0;
			bike_xspeed=0;
			bike_za=0;
			bike_zspeed=0;
			//cout<<"~"<<endl;
		}
	}else
	{
		bike_xa=0;
		bike_xspeed=0;
		bike_za=0;
		bike_zspeed=0;
	}
	if(bike_z+bike_zspeed<(_terrain->width())/2&& bike_z+bike_zspeed>-_terrain->width()/2&&temp>-7){
		temp=_terrain->getHeight(bike_x+_terrain->width()/2,bike_z+bike_zspeed+_terrain->length()/2);
		if(temp>-7)
		{
			bike_z+=bike_zspeed;
			bike_zspeed+=bike_za;
		}else{
			bike_xa=0;
			bike_xspeed=0;
			bike_za=0;
			bike_zspeed=0;
			//cout<<"~"<<endl;
		}
	}else{
		bike_xa=0;
		bike_xspeed=0;
		bike_za=0;
		bike_zspeed=0;
		//cout<<"~"<<endl;
	}
	bike_yspeed+=bike_ya-g;
	if(bike_y+bike_yspeed>=_terrain->getHeight(bike_x+_terrain->width()/2,bike_z+_terrain->length()/2))
	{
		bike_y+=bike_yspeed;
	}else{
		bike_yspeed=0;
		bike_y=_terrain->getHeight(bike_x+_terrain->width()/2,bike_z+_terrain->length()/2);
	}
	if(bike_y==_terrain->getHeight(bike_x+_terrain->width()/2,bike_z+_terrain->length()/2)&&bike_yspeed<0)
	{
		bike_yspeed=0;
		cout<<"~"<<endl;
	}

	bike_speed+=bike_a;
	bike_xa*=.95;
	bike_za*=.95;
	bike_xspeed*=.95;
	bike_zspeed*=.95;
	bike_speed*=.8;
	bike_a*=.8;
	//cout<<bike_speed<<endl;
	//	cout<<bike_x<<" "<<bike_y<<" "<<bike_z<<endl;
	glutPostRedisplay();
	glutTimerFunc(25, update, 0);

}

void handleKeypress2(int key, int x, int y) {	
	if(0){
		if (key == GLUT_KEY_UP)
			bike_za-=.01;
		if(key==GLUT_KEY_DOWN)
			bike_za+=.01;
		if (key == GLUT_KEY_LEFT)
			bike_xa-=.01;
		if(key==GLUT_KEY_RIGHT)
			bike_xa+=.01;
		//cout<<bike_angle*180/Pi<<endl;
	}else if(view==2||view==1||view==3||view==5||view==4)
	{
		//cout<<theta<<endl;
		bike_speed=sqrt(bike_xspeed*bike_xspeed+bike_zspeed*bike_zspeed)+.000000000001;
		if (key == GLUT_KEY_UP)
		{
			if(!play)
				system("aplay start.wav &");
			play=1;
			bike_a+=.001;
			bike_za+=.01*sin(bike_angle);
			bike_xa+=.01*cos(bike_angle);
			//			bike_za-=.01*sin(theta);
		}
		if(key==GLUT_KEY_DOWN)
		{
			bike_a-=.001;
			bike_za-=.01*sin(bike_angle);
			bike_xa-=.01*cos(bike_angle);
		}
		if (key == GLUT_KEY_LEFT)
		{
			rotl=1;
			bike_za-=.003*cos(bike_angle);
			bike_xa+=.003*sin(bike_angle);
		}
		if(key==GLUT_KEY_RIGHT){
			rotr=1;
			bike_za+=.003*cos(bike_angle);
			bike_xa-=.003*sin(bike_angle);
		}
	}

}
void rotlisten(int val){
	if(rotr)
	{	if(handle_rot>-25)
		handle_rot-=5;
		rotr=0;
	}else if(rotl)
	{	if(handle_rot<25)
		handle_rot+=5;
		rotl=0;
	}else{handle_rot*=.9;}
	glutTimerFunc(25, rotlisten, 0);
}
void handleKeypress1(unsigned char key, int x, int y) {
	if (key == 27||key=='q') {
		exit(0);     // escape key is pressed
	}else if(key<='5'&&key>='1')
		view=key-'0';
	if(key=='a')
		tilt-=1;
	if(key=='d')tilt+=1;
	//cout<<key<<endl;

}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(400, 400);
	glutCreateWindow("Assignment3");
	initRendering();
	w = glutGet(GLUT_SCREEN_WIDTH);
	h = glutGet(GLUT_SCREEN_HEIGHT);
	_terrain = loadTerrain("heightmap.bmp", 20);
	while(foss.sz<30)
		foss.push_back(make_pair(-_terrain->width()/2+rand()%(_terrain->width()),-_terrain->length()/2+rand()%(_terrain->length())));
	glutDisplayFunc(drawScene);
	glutReshapeFunc(handleResize);
	glutTimerFunc(25, update, 0);
	glutSpecialFunc(handleKeypress2);
	glutKeyboardFunc(handleKeypress1);	
	glutMotionFunc( mouse1 );
	glutPassiveMotionFunc( mouse1 );
	glutTimerFunc(25, rotlisten, 0);
	glutMouseFunc(mouse2);
	glutMainLoop();
	return 0;
}
